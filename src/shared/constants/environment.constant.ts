export enum EnvironmentConstant {
  APPLICATION_PORT = 'APPLICATION_PORT',
  JWT_PUBLIC_KEY = 'JWT_PUBLIC_KEY',
}
