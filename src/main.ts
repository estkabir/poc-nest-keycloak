import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { EnvironmentConstant } from './shared/constants/environment.constant';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const applicationPort = app
    .get(ConfigService)
    .get(EnvironmentConstant.APPLICATION_PORT);
  await app.listen(applicationPort);
  const url = await app.getUrl();
  const mainLogContext = 'Main bootstrap';
  Logger.log(`Api Server is running on ${url}`, mainLogContext);
}
bootstrap();
