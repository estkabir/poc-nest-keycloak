import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import ResponseLoginDto from './dtos/response.login.dto';

@Injectable()
export class AuthService {
  constructor(private http: HttpService) {}

  async login(username: string, password: string): Promise<ResponseLoginDto> {
    const { data } = await firstValueFrom(
      this.http.post(
        'http://localhost:8080/auth/realms/fiocruz_dev/protocol/openid-connect/token',
        new URLSearchParams({
          client_id: 'news-fiocruz',
          client_secret: 'HXyjg1BjGwErwhWpk5ZSKplsmg1IiFjJ',
          grant_type: 'password',
          username,
          password,
        }),
      ),
    );
    return data;
  }
}
