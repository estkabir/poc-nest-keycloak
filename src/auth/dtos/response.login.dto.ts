export default class ResponseLoginDto {
  private _access_token: string;
  private _refresh_token: string;
  private _token_type: string;
  private _session_state: string;
  private _scope: string;
  private _expires_in: bigint;
  private _refresh_expires_in: bigint;

  get access_token(): string {
    return this._access_token;
  }

  set access_token(value: string) {
    this._access_token = value;
  }

  get refresh_token(): string {
    return this._refresh_token;
  }

  set refresh_token(value: string) {
    this._refresh_token = value;
  }

  get token_type(): string {
    return this._token_type;
  }

  set token_type(value: string) {
    this._token_type = value;
  }

  get session_state(): string {
    return this._session_state;
  }

  set session_state(value: string) {
    this._session_state = value;
  }

  get scope(): string {
    return this._scope;
  }

  set scope(value: string) {
    this._scope = value;
  }

  get expires_in(): bigint {
    return this._expires_in;
  }

  set expires_in(value: bigint) {
    this._expires_in = value;
  }

  get refresh_expires_in(): bigint {
    return this._refresh_expires_in;
  }

  set refresh_expires_in(value: bigint) {
    this._refresh_expires_in = value;
  }
}
